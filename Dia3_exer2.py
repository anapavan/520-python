# Exercicio 2:
# Escreva uma calculadora utilizando funções
# Ela pergunta dois numeros, e da as opções de calculo.
# (soma, diferença, multiplicação, divisão)

#Definição da função Soma
def soma(n1,n2):
    return n1+n2

#Definição da função Subtração
def subtrair(n1,n2):
    return n1-n2

#Definição da função Divisão
def dividir(n1,n2):
    if(n2 != 0):
        return n1/n2
    print("Divisão por zero")

#Definição da função Multiplicação
def multiplicar(n1,n2):
    return n1*n2

# Solicitar os números e apresentar a opções de cálculo

n1 = int(input("Digite o primeiro número: "))
n2 = int(input("Digite o segundo número: "))

print("""
            Calculadora
            Selecione a operação desejada:

            1: Soma
            2: Subtração
            3: Divisão
            4: Multiplicação
            5: Sair   

            """)
    
opcao = input("Operação: ")
              
#Realizar a operação desejada
if opcao == '1':
    print(soma(n1,n2))
elif opcao == '2':
    print(subtrair(n1,n2))
elif opcao == '3':
    print(multiplicar(n1,n2))
elif opcao == '4':
    print(dividir(n1,n2))
elif opcao == '5':
    print("Bye Bye")
else:
    print("Valor inválido, digite a operação válida desejada")