# Exercicio 1:
# Escreva uma função que receba um nome e que tenha como saída uma saudação.

# O argumento da função deverá ser o nome, e saída deverá ser como a seguir:

# chamada da função: saudacao('Lalo')
# saída: 'Olá Lalo! Tudo bem com você?'

# Ao usar função primeiramente definir a função

def saudacao(nome):
    return "Olá {nome}! Tudo bem com você?"

#==============================================

nome = input("Informe um nome: ")
print(saudacao(nome))