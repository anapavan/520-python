# 1) Em muitos programas, nos é solicitado o preenchimento de algumas informações como
# nome, CPF, idade e unidade federativa. Escreva um script em Python que solicite as
# informações cadastrais mencionadas e que em seguida as apresente da seguinte forma:

# -----------------------------
# Confirmação de cadastro:
# Nome: Guido Van Rossum
# CPF: 999.888.777/66
# Idade: 65
# -----------------------------

nome = input("""Qual é o seu nome? """)
cpf = input("Qual é o seu CPF? ")
idade = input("Qual é a sua idade? ")

print(f"""
-----------------------------
Confirmação de cadastro:
Nome: {nome}
CPF: {cpf}
Idade: {idade}

-----------------------------
""")


# 2) Escreva um script em Python que receba dois números e que seja realizado as seguintes
# operações:
# • soma dos dois números
# • diferença dos dois números
# O resultado deverá ser apresentado conforme a seguir - no exemplo foram digitados os números
# 4 e 2:

# ------------------------------
# Soma: 4 + 2 = 6
# Diferença: 4 - 2 = 2

num1 = int(input("Digite o primeiro numero: "))
num2 = int(input("Digite o segundo numero: "))

soma = num1 + num2
diferenca = num1 - num2

print(f"""
------------------------------
Soma: {soma}
Diferença: {diferenca}
""")
